const theURL = 'https://thingproxy.freeboard.io/fetch/http://api.openweathermap.org/data/2.5/';
const API_KEY = process.env.VUE_APP_API_KEY;

export default function getWeather(parameter) {
  return fetch(`${theURL}${parameter}&units=metric&appid=${API_KEY}`,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((res) => res.json())
    .catch(() => Promise.reject(new Error('API failure')));
}
