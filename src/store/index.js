import Vue from 'vue';
import Vuex from 'vuex';
import getWeather from '../service';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    currentWeather: {},
    forecastWeather: {},
    error: '',
  },
  mutations: {
    SET_LOADING(state) {
      state.loading = !state.loading;
    },
    GET_CURRENT_WEATHER(state, payload) {
      state.currentWeather = { ...payload };
    },
    GET_FORECAST_WEATHER(state, payload) {
      state.forecastWeather = { ...payload };
    },
    SET_ERROR(state) {
      state.error = 'localisation non trouvée';
    },
    CLEAR_ERROR(state) {
      state.error = '';
    },
  },
  actions: {
    getCurrentWeather({ commit }, city) {
      commit('SET_LOADING');
      getWeather(city)
        .then((result) => {
          if (result.cod === '404') {
            commit('SET_LOADING');
            throw new Error('no city found');
          } else {
            commit('GET_CURRENT_WEATHER', result);
            commit('SET_LOADING');
            commit('CLEAR_ERROR');
          }
        })
        .catch((err) => {
          commit('SET_ERROR');
          throw new Error(err);
        });
    },
    getForecastWeather({ commit }, coordinates) {
      commit('SET_LOADING');
      getWeather(coordinates)
        .then((result) => {
          commit('GET_FORECAST_WEATHER', result);
          commit('SET_LOADING');
        })
        .catch((err) => { throw new Error(err); });
    },
  },
  getters: {
    current: (state) => state.currentWeather,
    forecast: (state) => state.forecastWeather,
    load: (state) => state.loading,
    error: (state) => state.error,
  },
});
