import Vue from 'vue';
import VueRouter from 'vue-router';
import CurrentWeather from '../views/CurrentWeather.vue';
import ForeCastWeather from '../views/ForeCastWeather.vue';
import HistoryWeather from '../views/HistoryWeather.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Current',
    component: CurrentWeather,
  },
  {
    path: '/forecast',
    name: 'ForeCast',
    component: ForeCastWeather,
  },
  {
    path: '/history',
    name: 'History',
    component: HistoryWeather,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
