import Chart from 'chart.js';

export default function drawChart(context, temperature, date) {
  const ctx = context;
  const chart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: date,
      datasets: [{
        label: 'Temperature ° C',
        borderColor: '#80b6f4',
        pointBorderColor: '#80b6f4',
        pointBackgroundColor: '#80b6f4',
        pointHoverBackgroundColor: '#80b6f4',
        pointHoverBorderColor: '#80b6f4',
        pointBorderWidth: 10,
        pointHoverRadius: 10,
        pointHoverBorderWidth: 1,
        pointRadius: 3,
        fill: false,
        borderWidth: 4,
        data: temperature,
      }],
    },
    options: {
      legend: {
        position: 'bottom',

      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: 'rgba(0,0,0,0.5)',
            fontStyle: 'bold',
            beginAtZero: true,
            maxTicksLimit: 5,
            padding: 20,
          },
          gridLines: {
            drawTicks: false,
            display: false,
          },
        }],
        xAxes: [{
          gridLines: { zeroLineColor: 'transparent' },
          ticks: {
            padding: 20,
            fontColor: 'rgba(0,0,0,0.5)',
            fontStyle: 'bold',
          },
        }],
      },
    },
  });
  return chart;
}
