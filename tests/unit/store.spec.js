import store from '@/store/index';

const weatherData = {
  coord: { lon: 2.35, lat: 48.85 },
  weather: [{
    id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04d',
  }],
  base: 'stations',
  main: {
    temp: 15.56,
    feels_like: 13.47,
    temp_min: 13.89,
    temp_max: 16.67,
    pressure: 1018,
    humidity: 58,
  },
  visibility: 10000,
  wind: { speed: 2.1, deg: 330 },
  clouds: { all: 87 },
  dt: 1594703382,
  sys: {
    type: 1,
    id: 6550,
    country: 'FR',
    sunrise: 1594699340,
    sunset: 1594756214,
  },
  timezone: 7200,
  id: 2988507,
  name: 'Paris',
  cod: 200,
};

global.getWeather = jest.fn(() => Promise.resolve(Promise.resolve(weatherData)));

global.fetch = jest.fn(() => Promise.resolve({
  json: () => Promise.resolve(weatherData),
}));

describe('store.js', () => {
  describe('mutations', () => {
    const { state } = store;
    it('should SET_LOADING ', () => {
      store.commit('SET_LOADING');
      expect(state.loading).toBeTruthy();
    });
    it('should CLEAR/SET ERROR', () => {
      store.commit('SET_ERROR');
      expect(state.error).toMatch('localisation non trouvée');
      store.commit('CLEAR_ERROR');
      expect(state.error).toMatch('');
    });
  });

  describe('actions', () => {
    it('should call getCurrentWeather', async () => {
      await store.dispatch('getCurrentWeather', 'weather?q=paris');
      setTimeout(() => {
        expect(store.getters.current).toMatchObject(weatherData);
      }, 0);
    });
  });
});
